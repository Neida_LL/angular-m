import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioDataI } from 'src/app/interface/usuario.interface';


@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  listUsuarios: UsuarioDataI[] = [
    {usuario: 'jperez', nombre: 'Juan', apellido: 'Perez', sexo: 'Masculino'},
    {usuario: 'mgomez', nombre: 'Maria', apellido: 'Gomez', sexo: 'Masculino'},
    {usuario: 'ngarcia', nombre: 'Nilda', apellido: 'Garcia', sexo: 'Femenino'},
    {usuario: 'kliop', nombre: 'Kevin', apellido: 'Liop', sexo: 'Masculino'},
    {usuario: 'hmarino', nombre: 'Hernan', apellido: 'Marino', sexo: 'Masculino'},
    {usuario: 'mmendizabal', nombre: 'Magdiel', apellido: 'Mendizabal', sexo: 'Femenino'}
  ];

  constructor(private router:Router) { }

  getUsuario(): UsuarioDataI[] {
    //slice retorna  una copia del array
    return this.listUsuarios.slice();
  }


  eliminarUsuario(usuario:string){
    this.listUsuarios =this.listUsuarios.filter(data => {
      return data.usuario!==usuario; 
    })
  }

  agregarUsuario(usuario:UsuarioDataI){
    this.listUsuarios.unshift(usuario);
  }

  buscarUsuario(id: string): UsuarioDataI{
    //o retorna un json {} vacio
    return this.listUsuarios.find(element => element.usuario === id) || {} as UsuarioDataI;
  }


  modificarUsuario(user: UsuarioDataI){
    this.eliminarUsuario(user.usuario);
    this.agregarUsuario(user);
  }


}
