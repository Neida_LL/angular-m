export interface UsuarioI {
  usuario:String;
  password:string;
}

export interface UsuarioDataI {
  usuario: string;
  nombre: string;
  apellido: string;
  sexo: string;
}
